import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
//component trebuie sa aibe url(html) si un css(style)
export class AuthComponent {

  viewType: String = "register"
  email: string = "";
  password = "";

  name: string = "";
  retypePassword: string = "";

  constructor(private router: Router, private authService: AuthService) {

  }

  onClearEmail(): void {
    console.log("before:" + this.email);
    this.email = "";
    console.log("after:" + this.email);

  }

  onRegisterClick(): void {
    this.viewType = "register"
  }

  onLoginClick(): void {
    this.viewType = "login"

//return type este intre parenteze rotunde si acadele
  }

  onSubmitLogin(): void {
    // this.router.navigate(["/", "dashboard"])
    this.authService.login(this.email, this.password).subscribe((response: any) => {
      console.log(response);
      this.router.navigate(["/", "dashboard"])
    });
  }

  onSubmitRegister(): void {
    if (this.password != this.retypePassword) {
      alert("Parolele sunt diferite!");

    }
    this.authService.register(this.name, this.email, this.password, this.retypePassword).subscribe((response: any) => {
      console.log(response);
      this.viewType = "login";
    });

  }
}
